describe('processDate() function', function(){
    it('processDate() function', function () {
        let testDate = {
            day: 11,
            dayOfWeek:"Friday",
            full:"Lottery numbers from Friday 11 May 2018",
            hour:21,
            minute:0,
            month:5,
            year:2018
        };
        let expectedDate = {
            long:"Friday 11 May 2018",
            numeric:"11.05.2018",
            short:"Fri 11 May",
            val:"11-05-2018"
        };
        expect(processDate(testDate)).toBe(expectedDate);
    });
});
describe('printWinners() function', function(){
    it('integerToRoman() function', function () {
        expect(integerToRoman(1)).toBe('I');
    });
    it('processWinners() function', function () {
        expect(processWinners(57)).toEqual('57');
    });
    it('processPrize() function', function () {
        expect(processPrize(70214740e-2)).toBe('€702,147.40');
    });
});


// printNumbers(key, allNumbers[key]);
