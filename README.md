# README #

On this README I'm listing the main steps I took building the page.

- I Installed al dev dependencies via npm commands. See package.json
- Using “Live Sass Compilar” plugin in Visual Studio Code. Settings:
    - Watching only styles.scss, excluding rest of scss files
    - Vendor prefixes: Last 2 versions of browsers with market share > 1%
- Babel/Webpack to transpile to ES5
    - Command used: npm run webpack
    - See webpack.config.js (I'm using the 'env' preset to allow transpiling generator functions)
- Http-server: python -m SimpleHTTPServer 9000
- For unit testing, I followed the jasmine official instructions and other tutorials, but unfortunately it fails when I execute it. 
    - Approach: using the html file “SpecRunner.html” https://github.com/jasmine/jasmine/releases