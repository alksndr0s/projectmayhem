// import "babel-polyfill";
// global variables
const config = {
    locale: 'com/en',
    lottery: 'euroJackpot',
    lotteryName: 'EuroJackpot'
}
let mainObj;

// print balls
let printNumbers = (key, numbers) => {
    const $ballsTarget = document.getElementById('results-balls');
    var ballDiv = document.createElement('div');
    ballDiv.className += 'number';
    if (key == 'euroNumbers') { ballDiv.className += ' extra'; }
    Array.from(numbers).forEach( n => {
        let thisBall = ballDiv.cloneNode(true);
        thisBall.innerHTML = n;
        $ballsTarget.appendChild(thisBall);
    });
}
// print winners rows and process its data
let printWinners = (odds, numbers) => {
    let integerToRoman = (num) =>{
        var result = [];
        const numbers = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1],
              roman = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];
        numbers.map((number, i) => { 
           while (num >= number) {
            result += roman[i];
            num -= number;
          }
         });
       return result;
    }      
    function* processNumbers() {
        yield [5, 2]; yield [5, 1]; yield [5, 0]; yield [4, 2]; yield [4, 1]; yield [4, 0]; yield [3, 2]; yield [2, 2]; yield [3, 1]; yield [3, 0]; yield [1, 2]; yield [2, 1] ;
    }
    let gen = processNumbers();
    let processPrize = (x) => {
        x =  Number.parseFloat(x).toFixed(2);
        return Intl.NumberFormat('en', { style: 'currency', currency: 'EUR' }).format(x);
    }
    let processWinners = (x) => {
        return Intl.NumberFormat().format(x)
    }
    let capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }
    odds = Object.values(odds);
    odds.sort(function(a, b) {
        var avalue = a.prize,
            bvalue = b.prize;
        if (avalue < bvalue) {
            return 1;
        }
        if (avalue > bvalue) {
            return -1;
        }
        return 0;
    });
    const numText = {
         num: capitalizeFirstLetter(Object.keys(numbers)[0]),
         euro: capitalizeFirstLetter(Object.keys(numbers)[1]),
    }
    const $winnersTarget = document.getElementById('results-winners');
    const rowDiv = document.createElement('div')
    rowDiv.innerHTML = '<div class="table-row table-row__results"><div class="tier"></div><div class="match"></div><div class="winners"></div><div class="amount"></div></div>';
    Object.keys(odds).map(function(key, index) {
        // console.log(odds[key]);
        if (odds[key].prize !== 0) {
            let thisRow = rowDiv.cloneNode(true);
            var prize = odds[key].prize.toString();
            let oddNums = gen.next().value;
            thisRow.getElementsByClassName('tier')[0].innerHTML = integerToRoman(parseInt(key)+1);
            thisRow.getElementsByClassName('match')[0].innerHTML = oddNums[0]+' '+numText.num+' + '+oddNums[1]+' '+numText.euro;
            thisRow.getElementsByClassName('winners')[0].innerHTML = processWinners(odds[key].winners)+'x';
            thisRow.getElementsByClassName('amount')[0].innerHTML =  processPrize(prize+'e-2');
            // console.log(thisRow);
            $winnersTarget.appendChild(thisRow);
        }
     });
}
// printing data in DOM targets
let collectAndPrint = (dateData) =>{
    let printData = (ref, src) => {
        let getOrdinal = (n) => {return["st","nd","rd"][((n+90)%100-10)%10-1]||"th"};
        if(ref == 'draw-number'){src += getOrdinal(src)};
        const targets = document.getElementsByName(ref);
        Array.from(targets).forEach(el => {
            el.innerHTML = src;
        });
    }
    let printable = [
        { ref: 'lotto-name', src: config.lotteryName},
        { ref: 'short-date', src: dateData.short},
        { ref: 'long-date', src: dateData.long},
        { ref: 'numeric-date', src: dateData.numeric},
        { ref: 'draw-number', src: mainObj.JPData.last.nr},
    ];
    function printDateSelect(callback) {
        const dateSelect = document.getElementById('draw-dates-select');
        dateSelect.options[dateSelect.options.length] = new Option(printable[1].src, dateData.val);
        callback();
    }
    printDateSelect(function(){
        printable.map( x => printData(x.ref, x.src));
    });
}
// process date
let processDate = (drawDate) => {
    let addZ = (n) => { return (n < 10 ? '0' : '') + n; }
    let dateStr = { short:'', long:'', numeric:'', val:'' };
    const shortMonths = ["Jan", "Feb", "Mar", 
    "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
    "Oct", "Nov", "Dec"];
    dateStr.short = drawDate.dayOfWeek.substring(0,3)+' '+drawDate.day+' '+shortMonths[drawDate.month-1];
    dateStr.long = drawDate.dayOfWeek+' '+drawDate.day+' '+shortMonths[drawDate.month-1]+' '+drawDate.year;
    dateStr.numeric = addZ(drawDate.day)+'.'+ addZ(drawDate.month)+'.'+drawDate.year;
    dateStr.val = drawDate.day+'-'+addZ(drawDate.month)+'-'+drawDate.year;
    return (dateStr); 
}
// retrieve data from JP API
let getJPData = $.ajax({
    url: 'https://www.lottoland.'+config.locale+'/api/drawings/'+config.lottery,    
    dataType: 'jsonp',
    success: function(data) {
        return data;
    },
    error: function(error) {
        console.error('Could not get lottery jackpots!', error);
    }
});
// Async functions
getJPData.then((JPData) => {
    mainObj = {JPData};
    // console.log('1. getJPDate - ',mainObj);
    return processDate(JPData.last.date);
}).then((dateData) => {
    mainObj.date = dateData;
    // console.log('2. processDate: ',dateData);
    collectAndPrint(dateData);
}).then(()=>{
    let allNumbers = {
        numbers: mainObj.JPData.last.numbers,
        euroNumbers: mainObj.JPData.last.euroNumbers
    };
    for (const key of Object.keys(allNumbers)) {
        printNumbers(key, allNumbers[key]);
    }
    return allNumbers;
}).then((allNumbers)=>{
    printWinners(mainObj.JPData.last.odds, allNumbers);
    // console.log('mainObj - ',mainObj);
}).catch(function(errors){
    console.log(errors);
});